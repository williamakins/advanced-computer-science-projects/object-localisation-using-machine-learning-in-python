import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import sys

sys.path.append('../')

import ErrorAnalysis.iou
from TrainingData.augmentation import draw_image_and_bboxes

def initialise_dataframe(path):
    """Load numpy arrays and condense in to a single dataframe.

    Parameters
    ----------
    path : str
        Path to directory containing arrays.

    Returns
    -------
    X_test : array
        Array of images encoded as 2D arrays of normalised RGB values.
    combined : DataFrame
        Pandas DataFrame containing combined arrays.

    """
    X_test = np.load(path.format('X_test.npy'))
    y_predict_box = np.load(path.format('y_predict_box.npy'))
    y_predict_class_labels = np.load(path.format('y_predict_class_labels.npy'), allow_pickle = True)
    y_predict_class = np.load(path.format('y_predict_class.npy'))
    y_predict_confidence_class = np.load(path.format('y_predict_confidence_class.npy'))
    y_true_box = np.load(path.format('y_true_box.npy'))
    y_true_class_labels = np.load(path.format('y_true_class_labels.npy'), allow_pickle = True)
    y_true_class = np.load(path.format('y_true_class.npy'))

    njf_true_bbox = [(int(i[0]), int(i[2]), int(i[1]), int(i[3])) for i in y_true_box]
    njf_pred_bbox = [(int(i[0]), int(i[2]), int(i[1]), int(i[3])) for i in y_predict_box]
    njf_true_class = [i for i in y_true_class]
    njf_pred_class = [i for i in y_predict_class]
    njf_true_label = [i[0] for i in y_true_class_labels]
    njf_pred_label = [i[0] for i in y_predict_class_labels]
    njf_pred_confid = [i for i in y_predict_confidence_class]

    d = {'true_bbox' : njf_true_bbox,
         'pred_bbox' : njf_pred_bbox,
         'true_class' : njf_true_class,
         'pred_class' : njf_pred_class,
         'true_label' : njf_true_label,
         'pred_label' : njf_pred_label,
         'pred_confidence' : njf_pred_confid}
        
    combined = pd.DataFrame(data=d)

    return X_test, combined

def find_successes(dataframe, test_class):
    """Find images for which the model has successfully predicted the 
       object class.

    Parameters
    ----------
    dataframe : DataFrame
        DataFrame containing combined arrays.

    Returns
    -------
    filtered : array
        Array of indices corresponding to X_test.

    """
    filtered = dataframe.loc[(dataframe['true_label'] == test_class) & (dataframe['pred_label'] == test_class)]

    return np.asarray(filtered.index)

def batch_iou(dataframe, indices, test_class):
    """Perform intersection over union calculation for all images in
       indices.

    Parameters
    ----------
    dataframe : DataFrame
        DataFrame containing combined arrays.
    indices : list
        List of indices of X_test to perform calculations on.
    test_class : str
        Object class for which batch processing is being performed on.

    Returns
    -------
    ious : array
        Array of intersection over union values.
    ious_hist_norm : array
        Normalised histogram of intersection over union values.
    ious_mean : float
        Mean value of ious.
    ious_std : array
        Standard deviation of ious.

    """
    pred_bboxes = [dataframe['pred_bbox'][i] for i in indices]
    true_bboxes = [dataframe['true_bbox'][i] for i in indices]
    zipped = zip(pred_bboxes, true_bboxes)

    ious = np.asarray([ErrorAnalysis.iou.main(p, t, 200) for (p, t) in zipped])
    ious_hist_norm = np.histogram(ious, bins=10, range=(0.0, 1.0))[0]/len(ious)
    ious_mean = np.mean(ious)
    ious_std = np.std(ious)

    np.savetxt('Data/Histograms/{}_ious_hist.txt'.format(test_class), ious_hist_norm)
    np.savetxt('Data/Raw/{}_ious.txt'.format(test_class), ious)

    return ious, ious_hist_norm, ious_mean, ious_std

def confidence_analysis(dataframe, class_key, indices, test_class):
    """Find confidence values for all images given by indices.

    Parameters
    ----------
    dataframe : DataFrame
        DataFrame containing combined arrays.
    class_key : list
        List of (index, class name) tuples
    indices : list
        List of indices of X_test to perform calculations on.
    test_class : str
        Object class for which batch processing is being performed on.

    Returns
    -------
    confidences : array
        Array of confidence values.
    confidences_hist_norm : array
        Normalised histogram of confidence values.
    confidences_mean : float
        Mean value of confidences.
    confidences_std : array
        Standard deviation of confidences.

    """
    index = [i for (i, c) in class_key if c == test_class][0]
    confidences = [dataframe['pred_confidence'][i][index] for i in indices]
    confidences_hist_norm = np.histogram(confidences, bins=10, range=(0.0, 1.0))[0]/len(confidences)
    confidences_mean = np.mean(confidences)
    confidences_std = np.std(confidences)
    
    np.savetxt('Data/Raw/{}_conf.txt'.format(test_class), confidences)
    np.savetxt('Data/Histograms/{}_conf_hist.txt'.format(test_class), confidences_hist_norm)

    return confidences, confidences_hist_norm, confidences_mean, confidences_std

def draw_all_plots(images, dataframe):
    """Find confidence values for all images given by indices.

    Parameters
    ----------
    dataframe : DataFrame
        DataFrame containing combined arrays.
    images : array
        Array of images in RGB format.

    """
    for i in range(len(results)):
        true_bbox = dataframe.iloc[i]['true_bbox']
        pred_bbox = dataframe.iloc[i]['pred_bbox']
        true_label = dataframe.iloc[i]['true_label']
        pred_label = dataframe.iloc[i]['pred_label']
        title = 'True label: {}, predicted label: {}'.format(true_label, pred_label)

        draw_image_and_bboxes(images[i],
                                bboxes=[true_bbox, pred_bbox],
                                savename='ImagesWithBboxes/{}/{}.png'.format(true_label, i),
                                title=title)


def main(results, class_key, test_class):
    """Perform iou and confidence analysis.

    Parameters
    ----------
    results : DataFrame
        DataFrame containing combined arrays.
    class_key : list
        List of (index, class name) tuples
    test_class : str
        Object class for which batch processing is being performed on.

    Returns
    -------
    iou_results : dict
        Dictionary of all iou quantities.
    confidence_results : dict
        Dictionary of all confidence quantities.

    """
    successes = find_successes(results, test_class)
    ious, ious_hist, ious_mean, ious_std = batch_iou(results, successes, test_class)
    confs, confs_hist, confs_mean, confs_std = confidence_analysis(results, class_key, successes, test_class)

    iou_results = dict([('raw_vals', ious),
                        ('histogram', ious_hist),
                        ('mean_val', ious_mean),
                        ('std_dev', ious_std)])
    conf_results = dict([('raw_vals', confs),
                         ('histogram', confs_hist),
                         ('mean_val', confs_mean),
                         ('std_dev', confs_std)])
    
    return iou_results, conf_results        

if __name__ == '__main__':

    
    path = '../Results/results_arrays/{}'
    images, results = initialise_dataframe(path)
    classes = ['aeroplane', 'bird', 'cat', 'dog']
    class_key = list(enumerate(classes))

    draw_all_plots(images, results)

    plane_iou_results, plane_conf_results = main(results, class_key, 'aeroplane')
    bird_iou_results, bird_conf_results = main(results, class_key, 'bird')
    cat_iou_results, cat_conf_results = main(results, class_key, 'cat')
    dog_iou_results, dog_conf_results = main(results, class_key, 'dog')

    column_names = ['iou_mean', 'iou_std', 'conf_mean', 'conf_std']

    with open('Data/results.txt', 'w') as f:
        f.write('class_name ' + ' '.join(column_names) + '\n')
        f.write('{} {:.3f}    {:.3f}   {:.3f}     {:.3f}\n'.format('aeroplane ',
                                                          plane_iou_results['mean_val'],
                                                          plane_iou_results['std_dev'],
                                                          plane_conf_results['mean_val'],
                                                          plane_conf_results['std_dev']))
        f.write('{} {:.3f}    {:.3f}   {:.3f}     {:.3f}\n'.format('bird      ',
                                                          bird_iou_results['mean_val'],
                                                          bird_iou_results['std_dev'],
                                                          bird_conf_results['mean_val'],
                                                          bird_conf_results['std_dev']))
        f.write('{} {:.3f}    {:.3f}   {:.3f}     {:.3f}\n'.format('cat       ',
                                                          cat_iou_results['mean_val'],
                                                          cat_iou_results['std_dev'],
                                                          cat_conf_results['mean_val'],
                                                          cat_conf_results['std_dev']))
        f.write('{} {:.3f}    {:.3f}   {:.3f}     {:.3f}\n'.format('dog       ',
                                                          dog_iou_results['mean_val'],
                                                          dog_iou_results['std_dev'],
                                                          dog_conf_results['mean_val'],
                                                          dog_conf_results['std_dev']))
    
    f.close()
