import numpy as np

def convert_array(dim, bbox):
    """Convert a bounding box tuple in to a 2d array filled with 0s if 
       the index is outside of the bounding box and the index itself if
       it is within the bounding box.

    Parameters
    ----------
    dim : int
        Size of one side of the image in pixels.
    bbox : tuple
        Bbox of format (xmin, xmax, ymin, ymax).

    Returns
    -------
    indices_2d : tuple
        2d array filled with indices corresponding to bounding box 
        limits.

    """

    indices_1d = np.asarray([i for i in range(dim*dim)])
    indices_2d = np.reshape(indices_1d, (dim,dim))

    for i in range(dim):
        for j in range(dim):
            if i < bbox[0] or i > bbox[1] or j < bbox[2] or j > bbox[3]:
                indices_2d[i,j] = 0

    return indices_2d

def intersection(pred_area, gold_area):
    """Find the number of intersecting pixels shared between two 2D arrays.

    Parameters
    ----------
    pred_area : array
        Array indicating area enclosed by predicted bounding box.
    true_area : array
        Array indicating area enclosed by true bounding box.

    Returns
    -------
    intersection_area : int
        The number of pixels common to both bounding boxes.

    """
    intersection = np.intersect1d(pred_area, gold_area)
    intersection_area = len(intersection) - 1

    return intersection_area 

def union(pred_area, gold_area):
    """Find the total number of pixels shared between two 2D arrays.

    Parameters
    ----------
    pred_area : array
        Array indicating area enclosed by predicted bounding box.
    true_area : array
        Array indicating area enclosed by true bounding box.

    Returns
    -------
    union_area : int
        The total number of pixels enclosed by both bounding boxes.

    """
    union = np.union1d(pred_area, gold_area)
    union_area = len(union) - 1
    
    return union_area

def intersection_over_union(intersection_area, union_area):
    """Divide intersection area by union area.

    Parameters
    ----------
    intersection_area : int
        The number of pixels common to both bounding boxes.
    union_area : int
        The total number of pixels enclosed by both bounding boxes.

    Returns
    -------
    iou : float
        Intersection over union value.

    """
    iou = intersection_area / union_area

    return iou

def main(pred_box, gold_box, dim, bbox_style='Nick'):

    if bbox_style == 'Liam':
        pred_box = (pred_box[0], pred_box[2], pred_box[1], pred_box[3])
        gold_box = (gold_box[0], gold_box[2], gold_box[1], gold_box[3])


    pred_area = convert_array(dim, pred_box)
    gold_area = convert_array(dim, gold_box)
    intersection_area = intersection(pred_area, gold_area)
    union_area = union(pred_area, gold_area)
    iou = intersection_over_union(intersection_area, union_area)

    return iou

if __name__ == '__main__':
    # Example routine
    test_pred = (1, 7, 2, 6)
    test_gold = (2, 8, 3, 7)
    dim = 10

    pred_area = convert_array(dim, test_pred)
    gold_area = convert_array(dim, test_gold)

    intersection_area = intersection(pred_area, gold_area)
    union_area = union(pred_area, gold_area)

    iou = intersection_over_union(intersection_area, union_area)

    print(pred_area)
    print(gold_area)
    print(intersection_area)
    print(union_area)
    print(iou)
