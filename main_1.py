import preprocessing_1
import implementation_1
import results_1

def main():
	devMode = True
	shouldPreProcess = False

	if shouldPreProcess == True:
		preprocessing_1.main(devMode)

	implementation_1.main(devMode)
	results_1.main(devMode)

if __name__ == "__main__":
	main()