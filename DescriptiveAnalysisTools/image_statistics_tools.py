import numpy as np
import os
import xml.etree.ElementTree as ET

def number_of_images(classes):
    """Find the number of images in the given classes.

    Parameters
    ----------
    classes : list
        The list of classes to operate on.

    Returns
    -------
    files : dict
        Dict, where keys = classes and vals = number of files in that 
        class.

    """
    files = {c : int(len(os.listdir('TrainingData/sorted_images/{}'.format(c)))/2) for c in classes}

    return files

def image_dimensions(classes = ['all']):
    """Find the dimensions of all images in the given classes and save
       as a tab-delimited text file.

    Parameters
    ----------
    classes : list
        The list of classes to operate on.

    """
    if not os.path.exists('DimensionData'):
        os.mkdir('DimensionData')
    
    if classes == ['all']:
        classes = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat',
                        'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike',
                        'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

    all_dimensions = []
    for c in classes:
        files = os.listdir('../TrainingData/sorted_images/{}'.format(c))
        annotations = [c for c in files if os.path.splitext(c)[1] == '.xml']
        sizes = []
        for a in annotations:
            tree = ET.parse('../TrainingData/sorted_images/{}/{}'.format(c, a))
            root = tree.getroot()
            sizes += [(d[0].text, d[1].text, d[2].text) for d in root.iter('size')]
            
        all_dimensions.append(sizes)

    labelled_sizes = dict(zip(classes, all_dimensions))

    for c in labelled_sizes:
        with open('DimensionData/{}_dimensions.txt'.format(c), mode = 'w') as f:
            for s in labelled_sizes[c]:
                f.write('{}\t{}\t{}\n'.format(s[0], s[1], s[2]))

def mean_bbox_sizes(classes):
    """Find the mean bounding box dimensions along with their area.

    Parameters
    ----------
    classes : list
        The list of classes to operate on.

    Returns
    -------
    mean_bbox_sizes : dict
        Dict, where keys = classes and vals = (mean xdim, mean ydim, area).

    """
    mean_bbox_sizes = {}
    for c in classes:

        path = '../PreprocessedImageData/{}/train_bbox_rgb_normal_out.npy'.format(c)
        bboxes = np.load(path)
        mean_xdim = int(np.mean([b[2]-b[0] for b in bboxes]))
        mean_ydim = int(np.mean([b[3]-b[1] for b in bboxes]))
        area = mean_xdim * mean_ydim
        print(c, ': ', (mean_xdim, mean_ydim, area))
        mean_bbox_sizes['{}'.format(c)] = (mean_xdim, mean_ydim, area)
    
    return mean_bbox_sizes

def find_singleobj_images(classes):
    """Find the number of images in the given classes which contain only
       one object.

    Parameters
    ----------
    classes : list
        The list of classes to operate on.

    Returns
    -------
    results : dict
        Dict, where keys = classes and vals = number of files in that
        class which contain only one object.

    """
    results = {}
    for c in classes:

        files = os.listdir('TrainingData/sorted_images/{}'.format(c))
        annotations = [c for c in files if os.path.splitext(c)[1] == '.xml']

        object_numbers = []

        for a in annotations:
            tree = ET.parse('TrainingData/sorted_images/{}/{}'.format(c, a))
            root = tree.getroot()
            object_numbers.append([child.tag for child in root].count('object'))

        num_single_objects = object_numbers.count(1)
        
        results['{}'.format(c)] = num_single_objects   

    return results

if __name__ == '__main__':

    classes = ['aeroplane', 'bird', 'cat', 'dog']

    mean_bbox_sizes(classes)