#file  -- implementation_1.py --

import numpy as np
import glob, os

def main(devMode):
	G_train_greyscale_X = []
	G_train_rgb_X = []
	train_bbox_y = []
	train_class_y = []

	DIR_PATH = os.path.dirname(os.path.realpath(__file__))

	folderPath = DIR_PATH + '/PreprocessedImageData/'

	#find all the folders within PreprocessedImageData
	selectedFolders = []
	for root, directories, files in os.walk(folderPath):
		selectedFolders += directories

	#loop through every folder
	for i in range(0, len(selectedFolders)):
		#only store the data from the folder titled person
		if selectedFolders[i] == "person":
			G_train_greyscale_X = np.load(folderPath + "/" + selectedFolders[i] + "/preprocessing_G_train_greyscale_X_out.npy", allow_pickle=True)
			G_train_rgb_X = np.load(folderPath + "/" + selectedFolders[i] + "/preprocessing_G_train_rgb_X_out.npy", allow_pickle=True)
			train_bbox_y = np.load(folderPath + "/" + selectedFolders[i] + "/preprocessing_train_bbox_y_out.npy", allow_pickle=True)
			train_class_y = np.load(folderPath + "/" + selectedFolders[i] + "/preprocessing_train_class_y_out.npy", allow_pickle=True)


#un-comment #main() below to run this script locally
main(True)