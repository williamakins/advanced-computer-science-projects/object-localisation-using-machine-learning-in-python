#file  -- preprocessing_1.py --

#needs keras installed and ideally TensorFlow https://keras.io/
#also install Pillow
#also install CUDA Toolkit https://developer.nvidia.com/cuda-10.1-download-archive-base *Make sure you're install CUDA Toolkit version 10.1

# TensorFlow and tf.keras
#import tensorflow as tf
#from tensorflow import keras

# Helper libraries
import math
import numpy as np
import matplotlib.pyplot as plt
import glob, os
import re

from decimal import *

# Pillow
import PIL
from PIL import Image, ImageOps

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.patches as patches

import xml.etree.ElementTree as ET

from sklearn.model_selection import train_test_split

#BEGIN DATA AUGMENTATION

def resize_bbox(image, new_dim=200, bbox=None):
    """Resize bounding box of image.

    Parameters
    ----------
    image : Image
        Image to be resized.
    new_dim : int
        Side length of square to fit image in to.
    mirror : bool
        Flip bbox on vertical axis?

    Returns
    -------
    new_bbox : tuple
        New bbox of format (xmin, xmax, ymin, ymax).

    """

    source_x, source_y = image.size
    factor_x = new_dim/source_x
    factor_y = new_dim/source_y

    new_bbox = (bbox[0]*factor_x,
                bbox[1]*factor_x,
                bbox[2]*factor_y,
                bbox[3]*factor_y)

    return new_bbox

def corners_from_bbox(bbox):
    """Create tuple of corners from bbox tuple.

    Parameters
    ----------
    bbox : tuple
        Bbox coordinates as given by xml file format.

    Returns
    -------
    corners : tuple
        Format ((x1, y1), (x1, y2), (x2, y1), (x2, y2)).

    """

    x1y1 = (bbox[0], bbox[2])
    x1y2 = (bbox[0], bbox[3])
    x2y1 = (bbox[1], bbox[2])
    x2y2 = (bbox[1], bbox[3])

    corners = (x1y1, x1y2, x2y1, x2y2)

    return corners

def bbox_from_corners(corners):
    """Create bbox from corners tuple.

    Parameters
    ----------
    corners : tuple
        Tuple of corners as given by corners_from_bbox.

    Returns
    -------
    bbox : tuple
        Bbox of format (xmin, xmax, ymin, ymax).

    """

    x1 = corners[1][0]
    x2 = corners[0][0]
    y1 = corners[0][1]
    y2 = corners[2][1]

    return (x1, x2, y1, y2)

def R(x, y, angle):
    """Rotates a point (x,y) around (0,0) by *angle*.

    Parameters
    ----------
    x : int
        x co-ordinate.
    y : int
        y co-ordinate.
    angle : int
        Angle (in degrees) to rotate the points by.

    Returns
    -------
    rot : tuple
        Co-ordinates of rotated (x,y) point.

    """

    theta = np.deg2rad(angle)
    rot = (x*np.cos(theta) - y*np.sin(theta),
            x*np.sin(theta) + y*np.cos(theta))

    return rot

def rotate_bbox(bbox, angle, new_dim):
    """Rotates a bbox by *angle* around point (new_dim/2,new_dim/2).

    Parameters
    ----------
    bbox : tuple
        Bbox of format (xmin, xmax, ymin, ymax).
    angle : int
        Angle (in degrees) to rotate the points by.
    new_dim : int
        Side length of square to fit image in to.

    Returns
    -------
    bbox : tuple
        Bbox of format (xmin, xmax, ymin, ymax).

    """

    corners = corners_from_bbox(bbox)
    translated = [(x - new_dim/2, y - new_dim/2) for (x,y) in corners]
    rotated = [R(t[0], t[1], angle) for t in translated]
    translated = [(x + new_dim/2, y + new_dim/2) for (x,y) in rotated]
    bbox = bbox_from_corners(translated)

    return bbox

def mirror_bbox(bbox, new_dim):
    """Mirror (flip along vertical axis) bbox.

    Parameters
    ----------
    bbox : tuple
        Bbox of format (xmin, xmax, ymin, ymax).
    new_dim : int
        Side length of square to fit image in to.

    Returns
    -------
    mirrored : tuple
        Mirrored box of format (xmin, xmax, ymin, ymax).

    """

    mirrored = [new_dim - bbox[0], new_dim - bbox[1], bbox[2], bbox[3]]

    #mirrored = (new_dim - bbox[0],
    #            new_dim - bbox[1],
    #            bbox[2],
    #            bbox[3])

    tmpPos = mirrored[0]
    mirrored[0] = mirrored[1]
    mirrored[1] = tmpPos

    
    return mirrored

def bbox_transforms(new_dim, new_bbox, rot_angle, mirror=False):
	"""Main function for combining bbox transforms.

	Parameters
	----------
	new_dim : int
	    Side length of square to fit image in to.
	new_bbox : tuple
	    Bbox input of format (xmin, xmax, ymin, ymax) after squishing to
	    new_dim sized square.
	rot_angle : int
	    Angle (in degrees) to rotate the points by.
	mirror : bool
	    Mirror bbox?

	Returns
	-------
	to_int : tuple
	    Fully transformed bbox of format (xmin, xmax, ymin, ymax).

	"""

	if mirror:
		new_bbox = mirror_bbox(new_bbox, new_dim)

	transformed = rotate_bbox(new_bbox, rot_angle, new_dim)

	to_int = tuple([int(b) for b in transformed])

	tmpArr = np.asarray(to_int)

	if rot_angle < 0:
		tmpPosX = tmpArr[0]
		tmpPosY = tmpArr[2]

		tmpArr[0] = tmpArr[1]
		tmpArr[1] = tmpPosX

		tmpArr[2] = tmpArr[3]
		tmpArr[3] = tmpPosY

	return tmpArr

def draw_image_and_bbox(image, bbox=None):
    """Plot image and bounding box to check correctness.

    Parameters
    ----------
    image : Image
        Image to be resized.
    bbox : tuple
        Bbox coordinates as given by xml file format.

    """

    _, ax = plt.subplots()
    ax.imshow(image)

    if bbox is not None:
        bbox = patches.Rectangle((bbox[0], bbox[1]),
                                  bbox[2] - bbox[0],
                                  bbox[3] - bbox[1],
                                  edgecolor = 'r',
                                  facecolor = 'none',
                                  linewidth = 2)
        ax.add_patch(bbox)

    plt.show()

#END DATA AUGMENTATION

def GenerateAndSaveImage(image, name, folder, bbox=None):
	_, ax = plt.subplots()
	ax.imshow(image)

	if bbox is not None:
		bbox = patches.Rectangle((bbox[0], bbox[1]),
								bbox[2] - bbox[0],
								bbox[3] - bbox[1],
								edgecolor = 'r',
								facecolor = 'none',
								linewidth = 2)
		ax.add_patch(bbox)

	plt.savefig('test/' + folder + "/" + str(name) + ".png")   # save the figure to file
	plt.close()    # close the figure window

#LoadData and ConvertToGreyScale functions adapted from code at https://developer.ibm.com/technologies/artificial-intelligence/articles/image-recognition-challenge-with-tensorflow-and-keras-pt1
def LoadData(fullPath, path, maxsize, NumImages):
	imageNum = 0

	train_img_main = [[],[],[],[],[],[],[],[],[],[],[],[]] #stores all the images with augmentations for a particular class
	train_bbox_main = [[],[],[],[],[],[],[],[],[],[],[],[]] #y dimension bounding boxes for all augmentations
	train_class_labels_main = [] #y dimension class names

	test_img_main = [[], []]
	test_bbox_main = [[], []]
	test_class_labels_main = []

	testPercent = 0.2

	os.chdir(fullPath)
	files = glob.glob("*.jpg")

	#calculate the number of usable files and store them

	allResizedImages = []
	allResizedBboxes = []
	allClassLabels = []
	imgCount = 0
	for i in range(0, len(files)):
		#parse the XML data and store the first bounding box into an array
		fileName = files[i][:-4] + ".xml"
		tree = ET.parse(fileName)
		root = tree.getroot()

		numOfBoundingBoxes = len(root.findall('object/name'))

		if (numOfBoundingBoxes == 1): #only use images that have a bounding box count of 1
			imgCount = imgCount + 1
			originalImg = Image.open(files[i])

			resizedImg = originalImg.resize((maxsize)) #open the image and resize it
			allResizedImages.append(resizedImg)

			bounds = []
			for boundingBox in root.findall('object/bndbox'):

				bounds.append(int(Decimal(boundingBox.find('xmin').text)))
				bounds.append(int(Decimal(boundingBox.find('xmax').text)))
				bounds.append(int(Decimal(boundingBox.find('ymin').text)))
				bounds.append(int(Decimal(boundingBox.find('ymax').text)))

				break

			resized_bbox = resize_bbox(originalImg, maxsize[0], bounds)
			allResizedBboxes.append(resized_bbox)

			for boundingBox in root.findall('object/name'):
				allClassLabels.append(boundingBox.text)
				break #find the first object class name

	#train_img, test_img = train_test_split(allResizedImages, test_size=testPercent, shuffle=True, random_state = 1)
	#train_bbox, test_bbox = train_test_split(allResizedBboxes, test_size=testPercent, shuffle=True, random_state = 1)

	train_img, test_img, train_bbox, test_bbox, train_class, test_class = train_test_split(allResizedImages, allResizedBboxes, allClassLabels, test_size=testPercent, shuffle=True)

	for f in range(0, len(train_img)):
		rgb_flipped = ImageOps.mirror(train_img[f]) # Flip on vertical axis

		rgb_resized_rotated_clockwise = train_img[f].rotate(-90)
		rgb_resized_rotated_anticlockwise = train_img[f].rotate(90)

		rgb_resized_flipped_rotated_clockwise = rgb_flipped.rotate(-90)
		rgb_resized_flipped_rotated_anticlockwise = rgb_flipped.rotate(90)
		
		greyscaleImg = train_img[f].convert('L') # original greyscale image

		greyscale_flipped = ImageOps.mirror(greyscaleImg) # Flip on vertical axis

		greyscale_resized_rotated_clockwise = greyscaleImg.rotate(-90)
		greyscale_resized_rotated_anticlockwise = greyscaleImg.rotate(90)

		greyscale_resized_flipped_rotated_clockwise = greyscale_flipped.rotate(-90)
		greyscale_resized_flipped_rotated_anticlockwise = greyscale_flipped.rotate(90)

		images = [np.asarray(train_img[f]),
				np.asarray(rgb_flipped),
				np.asarray(rgb_resized_rotated_clockwise),
				np.asarray(rgb_resized_rotated_anticlockwise),
				np.asarray(rgb_resized_flipped_rotated_clockwise),
				np.asarray(rgb_resized_flipped_rotated_anticlockwise),
				np.asarray(greyscaleImg),
				np.asarray(greyscale_flipped),
				np.asarray(greyscale_resized_rotated_clockwise),
				np.asarray(greyscale_resized_rotated_anticlockwise),
				np.asarray(greyscale_resized_flipped_rotated_clockwise),
				np.asarray(greyscale_resized_flipped_rotated_anticlockwise)]

		for i in range(0, len(images)):
			train_img_main[i].append(images[i])

		imageNum += 1

		#resized_bbox = resize_bbox(originalImg, maxsize[0], bounds)
		flipped_bbox = mirror_bbox(train_bbox[f], maxsize[0])

		resized_rotated_clockwise_bbox = bbox_transforms(maxsize[0], train_bbox[f], 90)
		resized_rotated_anticlockwise_bbox = bbox_transforms(maxsize[0], train_bbox[f], -90)

		resized_flipped_rotated_clockwise_bbox = bbox_transforms(maxsize[0], train_bbox[f], 90, mirror=True)
		resized_flipped_rotated_anticlockwise_bbox = bbox_transforms(maxsize[0], train_bbox[f], -90, mirror=True)

		bboxes = [np.asarray(train_bbox[f]),
				np.asarray(flipped_bbox),
				np.asarray(resized_rotated_clockwise_bbox),
				np.asarray(resized_rotated_anticlockwise_bbox),
				np.asarray(resized_flipped_rotated_clockwise_bbox),
				np.asarray(resized_flipped_rotated_anticlockwise_bbox),
				np.asarray(train_bbox[f]), #duplicated data for both greyscale and rgb images
				np.asarray(flipped_bbox),
				np.asarray(resized_rotated_clockwise_bbox),
				np.asarray(resized_rotated_anticlockwise_bbox),
				np.asarray(resized_flipped_rotated_clockwise_bbox),
				np.asarray(resized_flipped_rotated_anticlockwise_bbox)]

		#re-order all the bounds arrays to xmin, ymin, xmax, ymax
		
		for i in range(0, len(bboxes)):
			tmpBounds = [0, 0, 0, 0]

			tmpBounds[0] = bboxes[i][0]
			tmpBounds[1] = bboxes[i][2]
			tmpBounds[2] = bboxes[i][1]
			tmpBounds[3] = bboxes[i][3]

			bboxes[i] = tmpBounds

		#store all the bounding boxes in a large list
		for i in range(0, len(bboxes)):
			train_bbox_main[i].append(bboxes[i])

		#return early if we reach the image cap
		#if (NumImages != -1 and imageNum == NumImages):
			#return (img, bbox_labels, np.asarray(class_labels))

		print ("Processing train image " + files[f])

	#print(train_img[0])
	#print(test_img[0])

	#generate the test files without augmentation
	for f in range(0, len(test_img)):

		#for boundingBox in root.findall('object/name'):
		#	test_class_labels_main.append(boundingBox.text)
		#	break #find the first object clas name

		greyscaleImg = test_img[f].convert('L') # original greyscale image

		#resized_bbox = resize_bbox(test_img[f], maxsize[0], bounds)

		images = [np.asarray(test_img[f]),
				np.asarray(greyscaleImg)]

		for i in range(0, len(images)):
			test_img_main[i].append(images[i])

		#for boundingBox in root.findall('object/name'):
		#	test_class_labels_main.append(boundingBox.text)
		#	break #find the first object clas name

		bboxes = [np.asarray(test_bbox[f]),
				np.asarray(test_bbox[f])]

		for i in range(0, len(bboxes)):
			tmpBounds = [0, 0, 0, 0]

			tmpBounds[0] = bboxes[i][0]
			tmpBounds[1] = bboxes[i][2]
			tmpBounds[2] = bboxes[i][1]
			tmpBounds[3] = bboxes[i][3]

			bboxes[i] = tmpBounds

		#store all the bounding boxes in a large list
		for i in range(0, len(bboxes)):
			test_bbox_main[i].append(bboxes[i])

		print ("Processing test image " + files[f])

	return (train_img_main, train_bbox_main, np.asarray(train_class), test_img_main, test_bbox_main, np.asarray(test_class))

def cropImage(img, maxsize):
	WIDTH, HEIGHT = img.size
	if WIDTH != HEIGHT:
		m_min_d = min(WIDTH, HEIGHT)
		img = img.crop((0, 0, m_min_d, m_min_d))
	# Scale the image to the requested maxsize by Anti-alias sampling.
	img.thumbnail(maxsize, PIL.Image.ANTIALIAS)

	return np.asarray(img)

#def generateRGB(image, maxsize):
#	#img = Image.open(path)
#
#	#img = cropImage(img, maxsize)
#
#	return np.asarray(image)

#def generateGreyscale(image, maxsize):
#	img = image.convert('L') #convert image to 8-bit grayscale
#
#	#img = cropImage(img, maxsize)
#
#	return np.asarray(img)

def DisplayImage(imageData, maxSize):
	data = np.zeros((maxSize[1], maxSize[0], 3), dtype=np.uint8)
	data[0:200, 0:200] = [255, 0, 0] # red patch in upper left
	img = Image.fromarray(imageData, 'L')
	img.show()

def main(devMode):
	NUM_OF_IMAGES = -1 #-1 for no cap
	MAX_SIZE = 200, 200 #in pixels
	DIR_PATH = os.path.dirname(os.path.realpath(__file__))

	#number of folders to look through
	folderLimit = 20

	path = DIR_PATH + '/TrainingData/Sorted_Images/'

	train_augmentationExtensions = ['rgb_normal',
							  'rgb_flipped',
							  'rgb_normal_clockwise',
							  'rgb_normal_anticlockwise',
							  'rgb_flipped_clockwise',
							  'rgb_flipped_anticlockwise',
							  'greyscale_normal',
							  'greyscale_flipped',
							  'greyscale_normal_clockwise',
							  'greyscale_normal_anticlockwise',
							  'greyscale_flipped_clockwise',
							  'greyscale_flipped_anticlockwise']

	test_augmentationExtensions = ['rgb_normal',
								'greyscale_normal']

	#find all the folders
	selectedFolders = []
	for root, directories, files in os.walk(path):
		selectedFolders += directories

	for i in range(0, folderLimit):
		fullPath = path + selectedFolders[i]

		if selectedFolders[i] == "aeroplane" or selectedFolders[i] == "bird" or selectedFolders[i] == "cat" or selectedFolders[i] == "dog":
			#(G_train_greyscale_X, G_train_rgb_X, train_bbox_y, train_class_y) = LoadData(fullPath, path, MAX_SIZE, NUM_OF_IMAGES)

			(train_images, train_bbox, train_class, test_images, test_bbox, test_class) = LoadData(fullPath, path, MAX_SIZE, NUM_OF_IMAGES)


			#G_train_X.shape

			#(test_images, test_labels) = LoadData('/Users/yourself/image-recognition-tensorflow/chihuahua-muffin/test_set', maxsize)

			#DisplayImage(G_train_X[0], MAX_SIZE)

			#save the preprocessing output to a file for use within other python scripts
			os.chdir(DIR_PATH)

			outputFolder = "PreprocessedImageData"

			if not os.path.exists(outputFolder + '/{}'.format(selectedFolders[i])):
				os.mkdir(outputFolder + '/{}'.format(selectedFolders[i]))

			#trainImgCount = 0
			#for k in range(0, len(train_images[0])):
			#	GenerateAndSaveImage(train_images[0][k], trainImgCount, "train", train_bbox[0][k])
			#	trainImgCount = trainImgCount + 1

			#testImgCount = 0
			#for k in range(0, len(test_images[0])):
			#	GenerateAndSaveImage(test_images[0][k], testImgCount, "test", test_bbox[0][k])
			#	testImgCount = testImgCount + 1

			#save the train data
			for j in range(0, len(train_images)):
				print (train_bbox[j][0])
				draw_image_and_bbox(train_images[j][0], train_bbox[j][0])
				#for k in range(0, 5):
					#if trainImgCount < 5:
					#	print (train_bbox[0][k])
					#	draw_image_and_bbox(train_images[0][k], train_bbox[0][k])
					#	print("train")
					#	trainImgCount = trainImgCount + 1

				np.save(outputFolder + "/" + selectedFolders[i] + "/train_img_" + train_augmentationExtensions[j] + "_out", train_images[j], allow_pickle=True, fix_imports=True)
				np.save(outputFolder + "/" + selectedFolders[i] + "/train_bbox_" + train_augmentationExtensions[j] + "_out", train_bbox[j], allow_pickle=True, fix_imports=True)

			np.save(outputFolder + "/" + selectedFolders[i] + "/train_class_out", train_class, allow_pickle=True, fix_imports=True)
			
			#save the test data
			for j in range(0, len(test_images)):
				#for k in range(0, 5):
					#if testImgCount < 5:
					#	print (test_bbox[0][k])
					#	draw_image_and_bbox(test_images[0][k], test_bbox[0][k])
					#	print("test")
					#	testImgCount = testImgCount + 1

				np.save(outputFolder + "/" + selectedFolders[i] + "/test_img_" + test_augmentationExtensions[j] + "_out", test_images[j], allow_pickle=True, fix_imports=True)
				np.save(outputFolder + "/" + selectedFolders[i] + "/test_bbox_" + test_augmentationExtensions[j] + "_out", test_bbox[j], allow_pickle=True, fix_imports=True)

			np.save(outputFolder + "/" + selectedFolders[i] + "/test_class_out", test_class, allow_pickle=True, fix_imports=True)

			print(selectedFolders[i] + " preprocessed data saved to file")

#un-comment #main() below to run this script locally
main(True)